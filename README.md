The scripts to help Dark Data hunt
==================================

Detailed description of the whole procedure (to identify dark data and lost files) can be found at
the twiki [DDMDarkDataAndLostFiles](https://twiki.cern.ch/twiki/bin/view/AtlasComputing/DDMDarkDataAndLostFiles).
