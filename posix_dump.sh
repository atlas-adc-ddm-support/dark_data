#!/bin/sh
set -e

prefix='/storage/data'
spacetokens='atlasscratchdisk,atlaslocalgroupdisk,atlasuserdisk'

dprefix="$prefix"
dspacetokens="$spacetokens"

usage(){
    (
        printf "Usage: $0 [--prefix <prefix>] "
        printf "[--spacetokens <spctkn1[,spctkn2,...]>]\n"
        printf '\nOptions:\n'
        printf "  --prefix|-p      Prefix of the files in the local filesystem (full path)\n"
        printf "                   [default: \"$dprefix\"]\n"
        printf "  --spacetokens|-s Spacetokens to include in the dumps\n"
        printf "                   [default: \"$dspacetokens\"]\n"
    ) 1>&2
}

while [ $# -gt 0 ]; do
    case "$1" in
    --prefix|-p)
        shift
        prefix="$1"
        ;;
    --spacetokens|-s)
        shift
        spacetokens="$1"
        ;;
    --help|-h)
        usage
        exit 0
        ;;
     *)
        usage
        exit 1
    esac
    shift
done

prefix=/$(echo "$prefix" | sed -r 's@/+$@@' | sed -r 's@^/+@@')
discard=$(expr $(echo "$prefix" | tr -cd / | wc -m) + 3)

for spacetoken in $(echo $spacetokens | tr ',' ' ')
do
    find ${prefix}/${spacetoken} -type f | cut -d/ -f${discard}- | sed -r 's@^rucio/@@' > ${spacetoken}-dump.out
done 
